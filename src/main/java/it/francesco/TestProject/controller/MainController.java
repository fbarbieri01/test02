package it.francesco.TestProject.controller;

import it.francesco.TestProject.model.User;
import it.francesco.TestProject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class MainController {

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public String home(Model model){
        model.addAttribute("user", new User());
        model.addAttribute("welcome", "Welcome to intellij");
        model.addAttribute("all_users", userService.findAll());
        return "index";
    }

    @PostMapping("/add-user")
    public  String addUser(@ModelAttribute User user){
        userService.save(user);
        return "redirect:/";
    }

    @GetMapping("/user/{id}")
    public String showUser(@PathVariable int id, Model model){
        model.addAttribute("user", userService.findById(id));
        return "show_user";
    }

    @PostMapping("/update-user/{id}")
    public String updateUser(@PathVariable int id, @ModelAttribute User user){
        User oldUser = userService.findById(id);
        oldUser = user;
        userService.save(oldUser);
        return "redirect:/user/" + user.getId();
    }
}
