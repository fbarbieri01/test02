package it.francesco.TestProject.controller;

import it.francesco.TestProject.model.User;
import it.francesco.TestProject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ApiController {

    @Autowired
    private UserService userService;

    @GetMapping("/api/get-users")
    public Iterable<User> getUsers(){
        return userService.findAll();
    }
}
