package it.francesco.TestProject.repository;

import it.francesco.TestProject.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository <User, Integer>{

    @Query("select u from User u where u.id = ?1")
    User findById(int id);
}
